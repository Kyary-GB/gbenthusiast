package com.alecgdouglas.gbenthusiast.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;

import com.alecgdouglas.gbenthusiast.R;

public class ClearAuthorizationDialogFragment extends DialogFragment {
    private ClearAuthCallback mCallback;

    public ClearAuthorizationDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.clear_authorization_dialog_message)
                .setPositiveButton(R.string.clear, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mCallback != null) mCallback.onClearButton();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                final Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.requestFocus();
            }
        });
        return alertDialog;
    }

    public void setOnClearButtonCallback(ClearAuthCallback callback) {
        mCallback = callback;
    }

    public interface ClearAuthCallback {
        void onClearButton();
    }
}
