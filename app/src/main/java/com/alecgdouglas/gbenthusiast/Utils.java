/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.alecgdouglas.gbenthusiast;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.Format;

import java.util.Arrays;
import java.util.List;

/**
 * A collection of utility methods, all static.
 */
public class Utils {
    private static final String MANUFACTURER_AMAZON = "Amazon";
    private static final List<String> sLowEndDeviceModels;

    static {
        sLowEndDeviceModels = Arrays.asList("AFTM", "AFTB");
    }

    private Utils() {
    }

    /**
     * Shows a (long) toast.
     */
    public static void showToast(Context context, int resourceId) {
        Toast.makeText(context, context.getString(resourceId), Toast.LENGTH_LONG).show();
    }

    /**
     * Returns a user agent string based on the given application name and the library version.
     *
     * @param context         A valid context of the calling application.
     * @param applicationName String that will be prefix'ed to the generated user agent.
     * @return A user agent string generated using the applicationName and the library version.
     */
    public static String getUserAgent(Context context, String applicationName) {
        String versionName;
        try {
            String packageName = context.getPackageName();
            PackageInfo info = context.getPackageManager().getPackageInfo(packageName, 0);
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "?";
        }
        return applicationName + "/" + versionName + " (Linux;Android " + Build.VERSION.RELEASE +
                " " + Build.MODEL + ")" + " ExoPlayerLib/" + ExoPlayerLibraryInfo.VERSION;
    }

    public static String censorApiKey(String censor, String apiKey) {
        return apiKey == null ? censor : censor.replace(apiKey, "CENSORED_API_KEY");
    }

    public static boolean isAmazonDevice() {
        return Build.MANUFACTURER.equals(MANUFACTURER_AMAZON);
    }

    public static boolean isLowEndDevice() {
        return isAmazonDevice() && sLowEndDeviceModels.contains(Build.MODEL);
    }

    public static boolean supports1080p60() {
        return !isLowEndDevice();
    }

    public static boolean supportsFormat(Format format) {
        if (supports1080p60()) return true;

        // 1080p60 is not supported on this device
        return format.width < 1920 && format.height < 1080;
    }
}
