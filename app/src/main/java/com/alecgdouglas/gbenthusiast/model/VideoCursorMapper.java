/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.model;

import android.database.Cursor;
import android.support.v17.leanback.database.CursorMapper;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoEntry;
import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoPosition;

import java.text.ParseException;

/**
 * VideoCursorMapper maps a database Cursor to a Video object.
 */
public final class VideoCursorMapper extends CursorMapper {
    private static final String TAG = "VideoCursorMapper";

    private static int mGiantBombIdIndex;
    private static int mNameIndex;
    private static int mDeckIndex;
    private static int mTypeNameIndex;
    private static int mPublishDateIndex;
    private static int mLengthSecondsIndex;
    private static int mVideoUrlHdIndex;
    private static int mVideoUrlHighIndex;
    private static int mVideoUrlLowIndex;
    private static int mImageUrlTinyIndex;
    private static int mImageUrlMediumIndex;
    private static int mImageUrlSuperIndex;
    private static int mImageUrlIconIndex;
    private static int mPositionMillisIndex;

    private final String mApiKey;

    public VideoCursorMapper(String apiKey) {
        mApiKey = apiKey;
    }

    @Override
    protected void bindColumns(Cursor cursor) {
        mGiantBombIdIndex = cursor.getColumnIndex(VideoEntry.COLUMN_GIANT_BOMB_ID);
        mNameIndex = cursor.getColumnIndex(VideoEntry.COLUMN_NAME);
        mDeckIndex = cursor.getColumnIndex(VideoEntry.COLUMN_DECK);
        mTypeNameIndex = cursor.getColumnIndex(VideoEntry.COLUMN_TYPE_NAME);
        mPublishDateIndex = cursor.getColumnIndex(VideoEntry.COLUMN_PUBLISH_DATE);
        mLengthSecondsIndex = cursor.getColumnIndex(VideoEntry.COLUMN_LENGTH_SECONDS);
        mVideoUrlHdIndex = cursor.getColumnIndex(VideoEntry.COLUMN_VIDEO_URL_HD);
        mVideoUrlHighIndex = cursor.getColumnIndex(VideoEntry.COLUMN_VIDEO_URL_HIGH);
        mVideoUrlLowIndex = cursor.getColumnIndex(VideoEntry.COLUMN_VIDEO_URL_LOW);
        mImageUrlTinyIndex = cursor.getColumnIndex(VideoEntry.COLUMN_IMAGE_URL_TINY);
        mImageUrlMediumIndex = cursor.getColumnIndex(VideoEntry.COLUMN_IMAGE_URL_MEDIUM);
        mImageUrlSuperIndex = cursor.getColumnIndex(VideoEntry.COLUMN_IMAGE_URL_SUPER);
        mImageUrlIconIndex = cursor.getColumnIndex(VideoEntry.COLUMN_IMAGE_URL_ICON);
        mPositionMillisIndex = cursor.getColumnIndex(VideoPosition.COLUMN_VIDEO_POSITION_MILLIS);
    }

    @Override
    protected Object bind(Cursor cursor) {
        final Video.Builder builder = new Video.Builder(mApiKey, cursor.getLong(mGiantBombIdIndex),
                cursor.getString(mNameIndex)).withDeck(cursor.getString(mDeckIndex))
                .withVideoTypeName(cursor.getString(mTypeNameIndex))
                .withLengthSeconds(cursor.getLong(mLengthSecondsIndex))
                .withHdUrl(cursor.getString(mVideoUrlHdIndex))
                .withHighUrl(cursor.getString(mVideoUrlHighIndex))
                .withLowUrl(cursor.getString(mVideoUrlLowIndex))
                .withTinyImageUrl(cursor.getString(mImageUrlTinyIndex))
                .withMediumImageUrl(cursor.getString(mImageUrlMediumIndex))
                .withSuperImageUrl(cursor.getString(mImageUrlSuperIndex))
                .withIconImageUrl(cursor.getString(mImageUrlIconIndex))
                .withPositionMillis(cursor.getLong(mPositionMillisIndex));

        try {
            final String publishDateString = cursor.getString(mPublishDateIndex);
            builder.withPublishDate(Video.getDateFormat().parse(publishDateString));
        } catch (ParseException e) {
            Log.w(TAG, "Unable to parse date: " + cursor.getString(mPublishDateIndex), e);
        }

        return builder.build();
    }
}
