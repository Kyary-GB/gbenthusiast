package com.alecgdouglas.gbenthusiast.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VideoTypeTest {
    private static final String TEST_TYPE_NAME = "Type Name";
    private static final long TEST_TYPE_ID = 10L;
    private static final String TEST_TYPE_DECK = "This is a type deck";

    private final VideoType mVideoType =
            new VideoType(TEST_TYPE_NAME, TEST_TYPE_ID, TEST_TYPE_DECK);

    @Test
    public void testGetName() {
        assertThat(mVideoType.getName(), is(TEST_TYPE_NAME));
    }

    @Test
    public void testGetId() {
        assertThat(mVideoType.getId(), is(TEST_TYPE_ID));
    }

    @Test
    public void testGetDeck() {
        assertThat(mVideoType.getDeck(), is(TEST_TYPE_DECK));
    }
}
