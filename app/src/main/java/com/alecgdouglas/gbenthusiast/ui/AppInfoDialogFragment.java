package com.alecgdouglas.gbenthusiast.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.BuildConfig;
import com.alecgdouglas.gbenthusiast.R;

public class AppInfoDialogFragment extends DialogFragment {

    public AppInfoDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppInfoDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.app_info_dialog_fragment, container, false);

        final TextView messageView = (TextView) rootView.findViewById(R.id.message);
        final String message = getActivity().getResources()
                .getString(R.string.app_info_dialog_message, BuildConfig.VERSION_NAME);
        messageView.setText(message);

        final Button dismissButton = (Button) rootView.findViewById(R.id.dismiss_button);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dismiss();
            }
        });

        return rootView;
    }
}
