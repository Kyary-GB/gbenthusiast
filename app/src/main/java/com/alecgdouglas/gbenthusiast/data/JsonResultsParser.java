package com.alecgdouglas.gbenthusiast.data;

import org.json.JSONObject;

import java.util.List;

interface JsonResultsParser<N> {
    List<N> parse(JSONObject results);
}
