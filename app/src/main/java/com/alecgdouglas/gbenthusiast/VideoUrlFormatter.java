package com.alecgdouglas.gbenthusiast;

import android.net.Uri;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.alecgdouglas.gbenthusiast.model.Video;

public class VideoUrlFormatter {
    private static final String BITRATE_QUERY_PARAM_KEY = "b";
    private static final String API_KEY_QUERY_PARAM_KEY = "api_key";

    // Old video URLs have this domain that doesn't redirect properly to the new video URL.
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    static final String OLD_DOMAIN = "v.giantbomb.com";
    // New video URLs have this domain.
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    static final String NEW_DOMAIN = "giantbomb-pdl.akamaized.net";

    public static String formatUrl(String url, String apiKey, boolean isLive) {
        return formatUrl(url, apiKey, isLive, Utils.supports1080p60());
    }

    @VisibleForTesting
    static String formatUrl(String url, String apiKey, boolean isLive, boolean supports1080p60) {
        if (TextUtils.isEmpty(url) || Video.NULL_VIDEO_URL.equals(url)) return null;

        Uri uri = Uri.parse(url);
        final Uri.Builder builder;
        if (uri.getScheme() == null) {
            // Set the authority to the path in a new builder so when we set the scheme to https
            // it uses two forward slashes instead of one.
            // See: https://stackoverflow.com/a/36484689
            builder = new Uri.Builder();
            builder.encodedAuthority(uri.getPath()).encodedFragment(uri.getFragment())
                    .encodedQuery(uri.getQuery());
        } else {
            builder = uri.buildUpon();
        }
        uri = fixDomain(ensureHttps(builder.build()));
        if (isLive && supports1080p60) {
            uri = removeBitrateRestrictionQueryParam(uri);
        }
        uri = addApiKeyQueryParam(uri, apiKey);
        return uri.toString();
    }

    private static Uri ensureHttps(Uri uri) {
        return uri.buildUpon().scheme("https").build();
    }

    /**
     * Hacky workaround for broken redirects on old video URLs. Fingers crossed that GB doesn't
     * switch back to using their old domain for hosting videos, or if they do they at least
     * redirect properly.
     */
    private static Uri fixDomain(Uri uri) {
        if (uri.getHost().contains(OLD_DOMAIN)) {
            return Uri.parse(uri.toString().replace(OLD_DOMAIN, NEW_DOMAIN));
        }
        return uri;
    }

    private static Uri addApiKeyQueryParam(Uri uri, String apiKey) {
        final Uri.Builder builder = uri.buildUpon();
        builder.appendQueryParameter(API_KEY_QUERY_PARAM_KEY, apiKey);

        return builder.build();
    }

    private static Uri removeBitrateRestrictionQueryParam(Uri uri) {
        // Try to find and remove the bitrate limiting query parameter.
        final String bitrateQueryParamValue = uri.getQueryParameter(BITRATE_QUERY_PARAM_KEY);
        if (TextUtils.isEmpty(bitrateQueryParamValue)) {
            return uri;
        }
        final String fullQuery = uri.getQuery();
        if (uri.getQueryParameterNames().size() == 1) {
            // The bitrate restriction is the only query param so just remove the entire query
            // string, including the '?'.
            return Uri.parse(uri.toString().replace("?" + fullQuery, ""));
        }
        final String bitrateQueryParam =
                String.format("%s=%s", BITRATE_QUERY_PARAM_KEY, bitrateQueryParamValue);
        final String formattedQuery = fullQuery.replace("&" + bitrateQueryParam, "");
        return Uri.parse(uri.toString().replace(fullQuery, formattedQuery));
    }
}
