/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alecgdouglas.gbenthusiast.data;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoEntry;
import com.alecgdouglas.gbenthusiast.model.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The VideoDbBuilder is used to grab a JSON file from a server and parse the data
 * to be placed into a local database
 */
class VideoDbBuilder {
    private static final String TAG_RESULTS_ARRAY = "results";
    private static final String TAG_IMAGES_OBJECT = "image";
    private static final String TAG_GIANT_BOMB_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_DECK = "deck";
    private static final String TAG_VIDEO_CATEGORIES_ARRAY = "video_categories";
    private static final String TAG_VIDEO_CATEGORIES_NAME = "name";
    private static final String TAG_VIDEO_SHOW_OBJECT = "video_show";
    private static final String TAG_VIDEO_SHOW_TITLE = "title";
    private static final String TAG_TYPE_NAME = "video_type";
    private static final String TAG_PUBLISH_DATE = "publish_date";
    private static final String TAG_LENGTH_SECONDS = "length_seconds";
    private static final String TAG_VIDEO_URL_HD = "hd_url";
    private static final String TAG_VIDEO_URL_HIGH = "high_url";
    private static final String TAG_VIDEO_URL_LOW = "low_url";
    private static final String TAG_IMAGE_URL_TINY = "tiny_url";
    private static final String TAG_IMAGE_URL_MEDIUM = "medium_url";
    private static final String TAG_IMAGE_URL_SUPER = "super_url";
    private static final String TAG_IMAGE_URL_ICON = "icon_url";

    private static final String TAG = "VideoDbBuilder";

    /**
     * Takes the contents of a JSON object and populates the database
     *
     * @param json The JSON object results
     */
    public List<ContentValues> buildMedia(Context context, JSONObject json) {
        final JSONArray resultsArray = json.optJSONArray(TAG_RESULTS_ARRAY);
        if (resultsArray == null) {
            Log.i(TAG, "Unable to find results array in results JSON to build media");
            return new ArrayList<>(0);
        }

        final List<ContentValues> videosToInsert = new ArrayList<>();
        for (int i = 0; i < resultsArray.length(); i++) {
            try {
                final JSONObject videoJson = resultsArray.getJSONObject(i);
                final JSONObject imagesJson = videoJson.getJSONObject(TAG_IMAGES_OBJECT);

                final ContentValues videoValues = new ContentValues();
                videoValues
                        .put(VideoEntry.COLUMN_GIANT_BOMB_ID, videoJson.getLong(TAG_GIANT_BOMB_ID));
                videoValues.put(VideoEntry.COLUMN_NAME, videoJson.getString(TAG_NAME));
                videoValues.put(VideoEntry.COLUMN_DECK, videoJson.getString(TAG_DECK));
                videoValues.put(VideoEntry.COLUMN_TYPE_NAME, getVideoType(context, videoJson));
                videoValues
                        .put(VideoEntry.COLUMN_PUBLISH_DATE, videoJson.getString(TAG_PUBLISH_DATE));
                videoValues.put(VideoEntry.COLUMN_LENGTH_SECONDS,
                        videoJson.getLong(TAG_LENGTH_SECONDS));
                // Non-premium members don't get the HD URL - make it optional.
                if (videoJson.isNull(TAG_VIDEO_URL_HD)) {
                    // Avoid putting the string "null" into the database.
                    videoValues.put(VideoEntry.COLUMN_VIDEO_URL_HD, "");
                } else {
                    videoValues.put(VideoEntry.COLUMN_VIDEO_URL_HD,
                            videoJson.optString(TAG_VIDEO_URL_HD));
                }
                videoValues.put(VideoEntry.COLUMN_VIDEO_URL_HIGH,
                        videoJson.getString(TAG_VIDEO_URL_HIGH));
                videoValues.put(VideoEntry.COLUMN_VIDEO_URL_LOW,
                        videoJson.getString(TAG_VIDEO_URL_LOW));
                videoValues.put(VideoEntry.COLUMN_IMAGE_URL_TINY,
                        imagesJson.getString(TAG_IMAGE_URL_TINY));
                videoValues.put(VideoEntry.COLUMN_IMAGE_URL_MEDIUM,
                        imagesJson.getString(TAG_IMAGE_URL_MEDIUM));
                videoValues.put(VideoEntry.COLUMN_IMAGE_URL_SUPER,
                        imagesJson.getString(TAG_IMAGE_URL_SUPER));
                videoValues.put(VideoEntry.COLUMN_IMAGE_URL_ICON,
                        imagesJson.getString(TAG_IMAGE_URL_ICON));
                videosToInsert.add(videoValues);
            } catch (JSONException e) {
                Log.d(TAG, "Unable to get video from JSON at position " + i, e);
            }
        }
        return videosToInsert;
    }

    private String getVideoType(Context context, JSONObject videoJson) {
        final Set<String> videoTypes = new HashSet<>();
        try {
            // Get the video show if one exists
            if (!videoJson.isNull(TAG_VIDEO_SHOW_OBJECT)) {
                final JSONObject showJson = videoJson.getJSONObject(TAG_VIDEO_SHOW_OBJECT);
                if (!showJson.isNull(TAG_VIDEO_SHOW_TITLE)) {
                    final String showTitle = showJson.optString(TAG_VIDEO_SHOW_TITLE);
                    if (showTitle != null && !showTitle.isEmpty()) {
                        videoTypes.add(showTitle);
                    }
                }
            }

            // Get the video categories if they exist
            if (!videoJson.isNull(TAG_VIDEO_CATEGORIES_ARRAY)) {
                final JSONArray categoriesArray =
                        videoJson.optJSONArray(TAG_VIDEO_CATEGORIES_ARRAY);
                if (categoriesArray != null) {
                    for (int i = 0; i < categoriesArray.length(); i++) {
                        final JSONObject category = categoriesArray.getJSONObject(i);
                        if (category.isNull(TAG_VIDEO_CATEGORIES_NAME)) continue;
                        final String categoryName = category.optString(TAG_VIDEO_CATEGORIES_NAME);
                        if (categoryName != null && !categoryName.isEmpty()) {
                            videoTypes.add(categoryName);
                        }
                    }
                }
            }

            if (videoTypes.isEmpty() && !videoJson.isNull(TAG_TYPE_NAME)) {
                // Fall back on video type, which is a string containing a comma separated list of
                // video categories. This tag is deprecated but better than nothing at this
                // point since we haven't been able to get anything else.
                final String videoType = videoJson.optString(TAG_TYPE_NAME);
                if (videoType != null && !videoType.isEmpty()) {
                    videoTypes.add(videoType);
                }
            }
        } catch (JSONException e) {
            Log.d(TAG, "Failed to parse video type from JSON", e);
        }

        if (videoTypes.isEmpty()) {
            videoTypes.add(context.getString(R.string.no_category));
        }

        return TextUtils.join(Video.VIDEO_TYPE_DELIMITER, videoTypes);
    }
}
