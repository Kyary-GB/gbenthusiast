/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Copyright (c) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.ui;

import android.app.LoaderManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.DetailsFragment;
import android.support.v17.leanback.widget.Action;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.ClassPresenterSelector;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.DetailsOverviewLogoPresenter;
import android.support.v17.leanback.widget.DetailsOverviewRow;
import android.support.v17.leanback.widget.FullWidthDetailsOverviewRowPresenter;
import android.support.v17.leanback.widget.FullWidthDetailsOverviewSharedElementHelper;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnActionClickedListener;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.SparseArrayObjectAdapter;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.alecgdouglas.gbenthusiast.ApiKeyManager;
import com.alecgdouglas.gbenthusiast.DefaultQualityPrefUtils;
import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.Utils;
import com.alecgdouglas.gbenthusiast.VideoPositionRowUpdateUtils;
import com.alecgdouglas.gbenthusiast.data.VideoContract;
import com.alecgdouglas.gbenthusiast.data.VideoPositionDbUtils;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.alecgdouglas.gbenthusiast.model.VideoCursorMapper;
import com.alecgdouglas.gbenthusiast.model.VideoQuality;
import com.alecgdouglas.gbenthusiast.presenter.DetailsDescriptionPresenter;
import com.alecgdouglas.gbenthusiast.presenter.VideoCardPresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * VideoDetailsFragment extends DetailsFragment, a Wrapper fragment for leanback details screens.
 * It shows a detailed view of video and its metadata plus related videos.
 */
public class VideoDetailsFragment extends DetailsFragment
        implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "VideoDetailsFragment";
    private static final int NO_NOTIFICATION = -1;
    private final SparseArray<CursorObjectAdapter> mVideoCursorAdapters = new SparseArray<>();
    private Video mSelectedVideo;
    private ArrayObjectAdapter mAdapter;
    private BackgroundManager mBackgroundManager;
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private DetailsOverviewRow mDetailsOverviewRow;
    private final BroadcastReceiver mOnSavedVideoPositionUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Video updatedVideo =
                    (Video) intent.getSerializableExtra(VideoPositionDbUtils.VIDEO_KEY);

            // We know the video appears somewhere in the 'other videos' rows since they share
            // video types, so just pass in all headers as the 'always update' header names param.
            final List<String> headers = new ArrayList<>();
            for (int i = 0; i < mAdapter.size(); i++) {
                final Row row = (Row) mAdapter.get(i);
                if (row == null) continue;
                if (row instanceof ListRow && row.getHeaderItem() != null) {
                    headers.add(row.getHeaderItem().getName());
                }
            }
            VideoPositionRowUpdateUtils.updateRows(updatedVideo, mVideoCursorAdapters, mAdapter,
                    Collections.EMPTY_LIST, headers);
            // Force the details row to update itself. Setting to null first is the only way to
            // achieve this since the view won't be rebound unless setItem is called with a
            // different object than last time.
            mDetailsOverviewRow.setItem(null);
            mDetailsOverviewRow.setItem(updatedVideo);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prepareBackgroundManager();

        mSelectedVideo =
                (Video) getActivity().getIntent().getSerializableExtra(VideoDetailsActivity.VIDEO);

        if (mSelectedVideo == null) {
            Log.e(TAG, "Selected video is null");
            getActivity().finish();
        }

        boolean startVideoDirectly = getActivity().getIntent()
                .getBooleanExtra(VideoDetailsActivity.START_VIDEO_DIRECTLY, false);

        removeNotification(getActivity().getIntent()
                .getIntExtra(VideoDetailsActivity.NOTIFICATION_ID, NO_NOTIFICATION));
        setupAdapter();
        setupDetailsOverviewRow();
        setupOtherVideosRows();

        // When a Related Video item is clicked.
        setOnItemViewClickedListener(new ItemViewClickedListener());

        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(mOnSavedVideoPositionUpdatedReceiver, new IntentFilter(
                        VideoPositionDbUtils.ON_VIDEO_POSITION_UPDATED_BROADCAST_INTENT_KEY));

        if (startVideoDirectly) {
            startVideo(mSelectedVideo, DefaultQualityPrefUtils.getDefaultQuality(getActivity()));
        }
    }

    private void removeNotification(int notificationId) {
        if (notificationId != NO_NOTIFICATION) {
            NotificationManager notificationManager = (NotificationManager) getActivity()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationId);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateBackground(mSelectedVideo.getSuperImageUrl());
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity())
                .unregisterReceiver(mOnSavedVideoPositionUpdatedReceiver);
        super.onDestroy();
    }

    private void prepareBackgroundManager() {
        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.setAutoReleaseOnStop(true);
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground = getResources().getDrawable(R.drawable.default_background, null);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void updateBackground(String uri) {
        if (mBackgroundManager == null) return;
        Glide.with(this).load(uri).asBitmap().centerCrop().error(mDefaultBackground)
                .into(new SimpleTarget<Bitmap>(mMetrics.widthPixels, mMetrics.heightPixels) {
                    @Override
                    public void onResourceReady(Bitmap resource,
                                                GlideAnimation<? super Bitmap> glideAnimation) {
                        mBackgroundManager.setBitmap(resource);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        mBackgroundManager.setDrawable(errorDrawable);
                    }
                });
    }

    private void setupAdapter() {
        // Set detail background and style.
        FullWidthDetailsOverviewRowPresenter detailsPresenter =
                new FullWidthDetailsOverviewRowPresenter(new DetailsDescriptionPresenter(),
                        new MovieDetailsOverviewLogoPresenter());

        detailsPresenter.setBackgroundColor(
                ContextCompat.getColor(getActivity(), R.color.detail_background));
        detailsPresenter.setInitialState(FullWidthDetailsOverviewRowPresenter.STATE_HALF);

        if (!Utils.isLowEndDevice()) {
            // Hook up transition element.
            final FullWidthDetailsOverviewSharedElementHelper helper =
                    new FullWidthDetailsOverviewSharedElementHelper();
            helper.setSharedElementEnterTransition(getActivity(),
                    VideoDetailsActivity.SHARED_ELEMENT_NAME);
            detailsPresenter.setListener(helper);
            detailsPresenter.setParticipatingEntranceTransition(false);
            prepareEntranceTransition();
        }

        detailsPresenter.setOnActionClickedListener(new OnActionClickedListener() {
            @Override
            public void onActionClicked(Action action) {
                final long id = action.getId();
                if (id == VideoDetailsAction.PLAY_HD.getId() ||
                        id == VideoDetailsAction.PLAY_HD_DEFAULT.getId()) {
                    startVideo(mSelectedVideo, VideoQuality.HD);
                } else if (id == VideoDetailsAction.PLAY_HIGH.getId() ||
                        id == VideoDetailsAction.PLAY_HIGH_DEFAULT.getId()) {
                    startVideo(mSelectedVideo, VideoQuality.HIGH);
                } else if (id == VideoDetailsAction.PLAY_LOW.getId() ||
                        id == VideoDetailsAction.PLAY_LOW_DEFAULT.getId()) {
                    startVideo(mSelectedVideo, VideoQuality.LOW);
                } else if (action.getId() == VideoDetailsAction.MARK_COMPLETE.getId() &&
                        mSelectedVideo != null) {
                    VideoPositionDbUtils.updateCurrentPosition(getActivity(), mSelectedVideo,
                            mSelectedVideo.getDurationMillis());
                } else if (action.getId() == VideoDetailsAction.RESET_PROGRESS.getId() &&
                        mSelectedVideo != null) {
                    VideoPositionDbUtils.deleteSavedPosition(getActivity(), mSelectedVideo);
                } else if (action.getId() == VideoDetailsAction.WATCH_LIVE.getId() &&
                        mSelectedVideo != null) {
                    startVideo(mSelectedVideo, VideoQuality.HD);
                } else {
                    Toast.makeText(getActivity(), action.toString(), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        final ClassPresenterSelector presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(DetailsOverviewRow.class, detailsPresenter);
        presenterSelector.addClassPresenter(ListRow.class, new ListRowPresenter());
        mAdapter = new ArrayObjectAdapter(presenterSelector);
        setAdapter(mAdapter);
    }

    private void startVideo(Video video, VideoQuality quality) {
        final Intent intent = new Intent(getActivity(), PlaybackActivity.class);
        intent.putExtra(VideoDetailsActivity.VIDEO, video);
        intent.putExtra(VideoDetailsActivity.QUALITY, quality);
        Log.i(TAG, "Starting video with title '" + video.getName() + "' at " + quality.toString() +
                " quality");
        startActivity(intent);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final String whereClause = VideoContract.VideoEntry.COLUMN_TYPE_NAME + " LIKE ?";
        final String[] whereArgs = new String[] {
                "%" + args.getString(VideoContract.VideoEntry.COLUMN_TYPE_NAME) + "%"};
        final String orderByDateTime =
                "datetime(" + VideoContract.VideoEntry.COLUMN_PUBLISH_DATE + ") DESC";
        return new CursorLoader(getActivity(), VideoContract.VideoEntry.CONTENT_URI, null,
                whereClause, whereArgs, orderByDateTime);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            final CursorObjectAdapter adapter = mVideoCursorAdapters.get(loader.getId());
            if (adapter != null) {
                adapter.swapCursor(cursor);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        for (int i = 0; i < mVideoCursorAdapters.size(); i++) {
            int id = mVideoCursorAdapters.keyAt(i);
            mVideoCursorAdapters.get(id).changeCursor(null);
        }
        mVideoCursorAdapters.clear();
    }

    private void setupDetailsOverviewRow() {
        mDetailsOverviewRow = new DetailsOverviewRow(mSelectedVideo);

        final Resources res = getResources();
        int width = res.getDimensionPixelSize(R.dimen.detail_thumb_width);
        int height = res.getDimensionPixelSize(R.dimen.detail_thumb_height);

        Glide.with(this).load(mSelectedVideo.getSuperImageUrl()).asBitmap().dontAnimate()
                .error(R.drawable.error_thumbnail).into(new SimpleTarget<Bitmap>(width, height) {
            @Override
            public void onResourceReady(final Bitmap resource, GlideAnimation glideAnimation) {
                mDetailsOverviewRow.setImageBitmap(getActivity(), resource);
                startEntranceTransition();
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                mDetailsOverviewRow.setImageDrawable(errorDrawable);
                startEntranceTransition();
            }
        });

        final SparseArrayObjectAdapter adapter = new SparseArrayObjectAdapter();

        if (mSelectedVideo.isLive()) {
            adapter.set(VideoDetailsAction.WATCH_LIVE.getId(),
                    VideoDetailsAction.WATCH_LIVE.getAction(getActivity()));
        } else {
            // Add the default quality to the actions.
            VideoQuality defaultQuality = DefaultQualityPrefUtils.getDefaultQuality(getActivity());
            if (mSelectedVideo.hasUrlForQuality(defaultQuality)) {
                VideoDetailsAction actionForDefaultQuality = null;
                switch (defaultQuality) {
                    case HD:
                        actionForDefaultQuality = VideoDetailsAction.PLAY_HD_DEFAULT;
                        break;
                    case HIGH:
                        actionForDefaultQuality = VideoDetailsAction.PLAY_HIGH_DEFAULT;
                        break;
                    case LOW:
                        actionForDefaultQuality = VideoDetailsAction.PLAY_LOW_DEFAULT;
                        break;
                    default:
                        // We won't be able to add a default quality action so just set the default
                        // quality to null so all non-default qualities get added in the next steps.
                        defaultQuality = null;
                }
                if (actionForDefaultQuality != null) {
                    final Action action = actionForDefaultQuality.getAction(getActivity());
                    final String defaultedLabel = getResources()
                            .getString(R.string.defaulted_quality, action.getLabel1());
                    action.setLabel1(defaultedLabel);
                    adapter.set(actionForDefaultQuality.getId(), action);
                }
            }

            // Now add any other qualities that are not the default quality.
            if (!VideoQuality.HD.equals(defaultQuality) &&
                    mSelectedVideo.hasUrlForQuality(VideoQuality.HD)) {
                adapter.set(VideoDetailsAction.PLAY_HD.getId(),
                        VideoDetailsAction.PLAY_HD.getAction(getActivity()));
            }
            if (!VideoQuality.HIGH.equals(defaultQuality) &&
                    mSelectedVideo.hasUrlForQuality(VideoQuality.HIGH)) {
                adapter.set(VideoDetailsAction.PLAY_HIGH.getId(),
                        VideoDetailsAction.PLAY_HIGH.getAction(getActivity()));
            }
            if (!VideoQuality.LOW.equals(defaultQuality) &&
                    mSelectedVideo.hasUrlForQuality(VideoQuality.LOW)) {
                adapter.set(VideoDetailsAction.PLAY_LOW.getId(),
                        VideoDetailsAction.PLAY_LOW.getAction(getActivity()));
            }

            // And finally add in the video progress actions.
            adapter.set(VideoDetailsAction.MARK_COMPLETE.getId(),
                    VideoDetailsAction.MARK_COMPLETE.getAction(getActivity()));
            adapter.set(VideoDetailsAction.RESET_PROGRESS.getId(),
                    VideoDetailsAction.RESET_PROGRESS.getAction(getActivity()));
        }

        mDetailsOverviewRow.setActionsAdapter(adapter);

        mAdapter.add(mDetailsOverviewRow);
    }

    private void setupOtherVideosRows() {
        final String apiKey = ApiKeyManager.getApiKey(getActivity());
        if (apiKey == null) return;
        final VideoCursorMapper videoCursorMapper = new VideoCursorMapper(apiKey);
        // Generate related video rows.
        final List<String> videoTypeNames = mSelectedVideo.getVideoTypeNames();
        for (String videoTypeName : videoTypeNames) {
            final int id = videoTypeName.hashCode();

            final CursorObjectAdapter videoCursorAdapter =
                    new CursorObjectAdapter(new VideoCardPresenter());
            videoCursorAdapter.setMapper(videoCursorMapper);

            mVideoCursorAdapters.put(id, videoCursorAdapter);

            final Bundle args = new Bundle();
            args.putString(VideoContract.VideoEntry.COLUMN_TYPE_NAME, videoTypeName);
            getLoaderManager().initLoader(id, args, this);

            final HeaderItem header =
                    new HeaderItem(id, getString(R.string.other_in_category, videoTypeName));
            mAdapter.add(new ListRow(header, videoCursorAdapter));
        }
    }

    private enum VideoDetailsAction {
        // These appear in the actions row in the order they are listed here,
        // i.e. top enum -> leftmost action.
        // The DEFAULT video qualities must come before the non-default qualities.
        PLAY_HD_DEFAULT(R.string.play_hd),
        PLAY_HIGH_DEFAULT(R.string.play_high),
        PLAY_LOW_DEFAULT(R.string.play_low),
        PLAY_HD(R.string.play_hd),
        PLAY_HIGH(R.string.play_high),
        PLAY_LOW(R.string.play_low),
        // The progress actions must come after
        // the video qualities.
        MARK_COMPLETE(R.string.mark_complete),
        RESET_PROGRESS(R.string.reset_progress),
        WATCH_LIVE(R.string.watch_live);

        private final int mStringId;

        VideoDetailsAction(int stringId) {
            mStringId = stringId;
        }

        public int getId() {
            return ordinal();
        }

        public Action getAction(Context context) {
            return new Action(getId(), context.getResources().getString(mStringId));
        }
    }

    static class MovieDetailsOverviewLogoPresenter extends DetailsOverviewLogoPresenter {

        @Override
        public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
            ImageView imageView = (ImageView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lb_fullwidth_details_overview_logo, parent, false);

            Resources res = parent.getResources();
            int width = res.getDimensionPixelSize(R.dimen.detail_thumb_width);
            int height = res.getDimensionPixelSize(R.dimen.detail_thumb_height);
            imageView.setLayoutParams(new ViewGroup.MarginLayoutParams(width, height));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            return new ViewHolder(imageView);
        }

        @Override
        public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
            DetailsOverviewRow row = (DetailsOverviewRow) item;
            ImageView imageView = ((ImageView) viewHolder.view);
            imageView.setImageDrawable(row.getImageDrawable());
            if (isBoundToImage((ViewHolder) viewHolder, row)) {
                MovieDetailsOverviewLogoPresenter.ViewHolder vh =
                        (MovieDetailsOverviewLogoPresenter.ViewHolder) viewHolder;
                vh.getParentPresenter().notifyOnBindLogo(vh.getParentViewHolder());
            }
        }

        static class ViewHolder extends DetailsOverviewLogoPresenter.ViewHolder {
            public ViewHolder(View view) {
                super(view);
            }

            public FullWidthDetailsOverviewRowPresenter getParentPresenter() {
                return mParentPresenter;
            }

            public FullWidthDetailsOverviewRowPresenter.ViewHolder getParentViewHolder() {
                return mParentViewHolder;
            }
        }
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {
            if (item instanceof Video) {
                final Video video = (Video) item;
                final Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                final Bundle bundle = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(getActivity(),
                                ((ImageCardView) itemViewHolder.view).getMainImageView(),
                                VideoDetailsActivity.SHARED_ELEMENT_NAME).toBundle();
                getActivity().startActivity(intent, bundle);
            }
        }
    }
}
