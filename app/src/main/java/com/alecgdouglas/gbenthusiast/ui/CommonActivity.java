package com.alecgdouglas.gbenthusiast.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.amazonaws.mobileconnectors.amazonmobileanalytics.InitializationException;
import com.amazonaws.mobileconnectors.amazonmobileanalytics.MobileAnalyticsManager;

/**
 * This parent class contains common methods that run in every activity such as mobile analytics
 * reporting.
 */
public abstract class CommonActivity extends FragmentActivity {
    private static MobileAnalyticsManager sAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            sAnalytics = MobileAnalyticsManager.getOrCreateInstance(this.getApplicationContext(),
                    // AWS Mobile Analytics App ID
                    "ec39142900df4aa98694dfba43bc9d38",
                    // AWS Cognito Identity Pool ID
                    "us-east-1:6a0ab3a5-7fe9-4ee3-b609-8d270199e50c");
        } catch (InitializationException e) {
            Log.e(getClass().getName(), "Failed to initialize Amazon Mobile Analytics", e);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sAnalytics != null) {
            sAnalytics.getSessionClient().pauseSession();
            // Attempt to send any events that have been recorded to the Mobile Analytics service.
            sAnalytics.getEventClient().submitEvents();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sAnalytics != null) {
            sAnalytics.getSessionClient().resumeSession();
        }
    }
}
