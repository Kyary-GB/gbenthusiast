package com.alecgdouglas.gbenthusiast;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class VideoUrlFormatterTest {
    private static final String SECURE_BASE_URL = "https://bitdash-a.akamaihd" +
            ".net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8";
    private static final String API_KEY = "abc123";

    @Test
    public void testRemoveSoloBitrateRestrictionQueryParam() {
        final String initialUrl = SECURE_BASE_URL + "?b=1-5000";

        final String formattedUrl = VideoUrlFormatter.formatUrl(initialUrl, API_KEY, true, true);
        assertEquals(SECURE_BASE_URL + "?api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testRemoveBitrateRestrictionQueryParamWithOtherParams() {
        final String initialUrl = SECURE_BASE_URL + "?a=300&b=1-5000&howdy=hello";

        final String formattedUrl = VideoUrlFormatter.formatUrl(initialUrl, API_KEY, true, true);
        assertEquals(SECURE_BASE_URL + "?a=300&howdy=hello&api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testBitrateRestrictionUntouchedIfNotLive() {
        final String initialUrl = SECURE_BASE_URL + "?b=1-5000";

        final String formattedUrl = VideoUrlFormatter.formatUrl(initialUrl, API_KEY, false, true);
        assertEquals(initialUrl + "&api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testBitrateRestrictionUntouchedIf1080p60NotSupported() {
        final String initialUrl = SECURE_BASE_URL + "?b=1-5000";

        final String formattedUrl = VideoUrlFormatter.formatUrl(initialUrl, API_KEY, true, false);
        assertEquals(initialUrl + "&api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testNoBitrateRestriction() {
        final String formattedUrl =
                VideoUrlFormatter.formatUrl(SECURE_BASE_URL, API_KEY, true, true);
        assertEquals(SECURE_BASE_URL + "?api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testNullUrlReturnsNull() {
        final String formattedUrl = VideoUrlFormatter.formatUrl(null, API_KEY, true, true);
        assertNull(formattedUrl);
    }

    @Test
    public void testNullStringUrlReturnsNull() {
        final String formattedUrl = VideoUrlFormatter.formatUrl("null", API_KEY, true, true);
        assertNull(formattedUrl);
    }

    @Test
    public void testForceHttps() {
        final String expectedFormattedSecureUrl = SECURE_BASE_URL + "?api_key=" + API_KEY;

        // Ensure insecure URLs get made to use https
        final String insecureBaseUrl = SECURE_BASE_URL.replace("https://", "http://");
        final String formattedInsecureUrl =
                VideoUrlFormatter.formatUrl(insecureBaseUrl, API_KEY, false, true);
        assertEquals(expectedFormattedSecureUrl, formattedInsecureUrl);

        // Ensure already secure URLs continue to use https
        final String formattedSecureUrl =
                VideoUrlFormatter.formatUrl(SECURE_BASE_URL, API_KEY, false, true);
        assertEquals(expectedFormattedSecureUrl, formattedSecureUrl);
    }

    @Test
    public void testForceHttpsWhenNoSchemePresent() {
        final String noSchemeUrl = SECURE_BASE_URL.replace("https://", "");
        final String formattedUrl = VideoUrlFormatter.formatUrl(noSchemeUrl, API_KEY, false, true);
        assertEquals(SECURE_BASE_URL + "?api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testForceHttpsWhenNoSchemePresentWithQuery() {
        final String noSchemeUrl = SECURE_BASE_URL.replace("https://", "") + "?b=200";
        final String formattedUrl = VideoUrlFormatter.formatUrl(noSchemeUrl, API_KEY, false, true);
        assertEquals(SECURE_BASE_URL + "?b=200&api_key=" + API_KEY, formattedUrl);
    }

    @Test
    public void testFixDomain() {
        final String oldUrl = "https://" + VideoUrlFormatter.OLD_DOMAIN + "/video/fun.mp4";
        final String expectedUrl = "https://" + VideoUrlFormatter.NEW_DOMAIN + "/video/fun.mp4?api_key=" + API_KEY;

        assertEquals(expectedUrl, VideoUrlFormatter.formatUrl(oldUrl, API_KEY, false, true));
    }
}
