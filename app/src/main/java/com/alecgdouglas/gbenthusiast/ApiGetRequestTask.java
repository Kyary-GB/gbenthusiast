package com.alecgdouglas.gbenthusiast;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class ApiGetRequestTask
        extends AsyncTask<ApiGetRequestParams, Void, Map<ApiGetRequestParams, JSONObject>> {
    private static final String TAG = "ApiGetRequestTask";
    private static final String GIANT_BOMB_API_URL_STRING = "https://www.giantbomb.com/api/";

    public ApiGetRequestTask() {
    }

    public boolean isRunning() {
        return getStatus() == Status.RUNNING;
    }

    @Override
    protected Map<ApiGetRequestParams, JSONObject> doInBackground(ApiGetRequestParams... params) {
        return getResults(params);
    }

    public void executeInParallel(List<ApiGetRequestParams> params) {
        Log.d(TAG, "executeInParallel - spawning " + params.size() + " async tasks");
        final CountDownLatch latch = new CountDownLatch(params.size());
        final Map<ApiGetRequestParams, JSONObject> allResponses =
                new ConcurrentHashMap<>(params.size());
        for (ApiGetRequestParams param : params) {
            new ApiGetRequestTask() {
                @Override
                protected void onPostExecute(Map<ApiGetRequestParams, JSONObject> responses) {
                    latch.countDown();
                    Log.d(TAG, "executeInParallel#onPostExecute - new latch count = " +
                            latch.getCount());
                    for (Map.Entry<ApiGetRequestParams, JSONObject> entry : responses.entrySet()) {
                        // Must filter out null values/keys otherwise we get a NPE here
                        // (ConcurrentHashMap does not allow null keys or values).
                        if (entry.getKey() != null && entry.getValue() != null) {
                            allResponses.put(entry.getKey(), entry.getValue());
                        }
                    }

                    if (latch.getCount() == 0) {
                        Log.d(TAG, "Final request complete, calling onPostExecute with responses");
                        ApiGetRequestTask.this.onPostExecute(allResponses);
                    }
                }
            }.executeOnExecutor(THREAD_POOL_EXECUTOR, param);
        }
    }

    /**
     * Allows subclasses to override the endpoint URL that is used for the request.
     *
     * @return The full endpoint URL - just missing the specific API to query and query params.
     */
    protected String getEndpointUrl() {
        return GIANT_BOMB_API_URL_STRING;
    }

    private Map<ApiGetRequestParams, JSONObject> getResults(ApiGetRequestParams... params) {
        final Map<ApiGetRequestParams, JSONObject> results = new HashMap<>(params.length);
        for (ApiGetRequestParams param : params) {
            results.put(param, makeApiGetRequest(param));
        }
        return results;
    }

    private JSONObject makeApiGetRequest(ApiGetRequestParams params) {
        final String api = params.getApi();
        final String formattedParams = params.getFormattedParams();
        final String urlString = getEndpointUrl() + api + "?" + formattedParams;

        URL url;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.w(TAG,
                    "Malformed validate URL: " + Utils.censorApiKey(urlString, params.getApiKey()),
                    e);
            return null;
        }
        Log.d(TAG, "Making API GET request for following URL: " +
                Utils.censorApiKey(urlString, params.getApiKey()));
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
        } catch (IOException e) {
            Log.w(TAG, "IOException encountered while opening connection to get API key", e);
            return null;
        }

        if (isCancelled()) return null;

        final StringBuilder responseBuilder = new StringBuilder();
        try (final InputStream responseStream = connection.getInputStream();
             final InputStreamReader reader = new InputStreamReader(responseStream);
             final BufferedReader bufferedReader = new BufferedReader(reader)) {
            final int status = connection.getResponseCode();
            Log.d(TAG, "API GET request response code: " + status);
            switch (status) {
                case 200:
                case 201:
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        if (isCancelled()) return null;
                        responseBuilder.append(line).append("\n");
                    }
                    break;
                default:
                    Log.i(TAG, "Response code was " + status + ", giving up on GET request");
                    return null;
            }
        } catch (IOException e) {
            Log.w(TAG, "IOException encountered while getting response from getting API key", e);
            return null;
        }

        final String jsonResponse = responseBuilder.toString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        if (isCancelled()) return null;

        JSONObject json = null;
        try {
            json = new JSONObject(jsonResponse);
        } catch (JSONException e) {
            Log.w(TAG, "Unable to parse response string as JSON", e);
        }
        return json;
    }
}
