package com.alecgdouglas.gbenthusiast.presenter;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v17.leanback.widget.ImageCardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.alecgdouglas.gbenthusiast.ui.VideoCard;
import com.bumptech.glide.Glide;

public class VideoCardPresenter extends CardPresenter {
    private final String mProgressTextTag = "progress_text";
    private final String mDurationTextTag = "duration_text";
    private final VideoCard mVideoCard = new VideoCard();

    public VideoCardPresenter() {
        super();
    }

    public VideoCardPresenter(CardSize cardSize) {
        super(cardSize);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        final ViewHolder viewHolder = super.onCreateViewHolder(parent);
        final ImageCardView cardView = (ImageCardView) viewHolder.view;
        final TextView contentText =
                (TextView) cardView.findViewById(android.support.v17.leanback.R.id.content_text);
        contentText.getLayoutParams().width = RelativeLayout.LayoutParams.WRAP_CONTENT;

        final RelativeLayout infoField = (RelativeLayout) cardView
                .findViewById(android.support.v17.leanback.R.id.info_field);

        final TextView progressText = new TextView(cardView.getContext(), null,
                android.support.v17.leanback.R.attr.imageCardViewContentStyle);
        progressText.setId(View.generateViewId());
        progressText.setTag(mProgressTextTag);

        final RelativeLayout.LayoutParams progressParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        progressParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        progressParams.addRule(RelativeLayout.ALIGN_TOP, contentText.getId());
        infoField.addView(progressText, progressParams);

        final TextView durationText = new TextView(cardView.getContext(), null,
                android.support.v17.leanback.R.attr.imageCardViewContentStyle);
        durationText.setId(View.generateViewId());
        durationText.setTag(mDurationTextTag);
        durationText.setPadding(durationText.getPaddingLeft(), durationText.getPaddingTop(),
                infoField.getPaddingRight(), durationText.getPaddingBottom());

        final RelativeLayout.LayoutParams durationParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        durationParams.addRule(RelativeLayout.LEFT_OF, progressText.getId());
        durationParams.addRule(RelativeLayout.ALIGN_TOP, contentText.getId());
        infoField.addView(durationText, durationParams);

        return viewHolder;
    }

    @Override
    public void onBindImageCardView(ImageCardView cardView, Object item) {
        if (item instanceof DrawableItem){
            ((DrawableItem) item).drawItem(cardView, item);
        } else if (item instanceof Video) {
            mVideoCard.drawItem(cardView, item);
        }
    }
}
